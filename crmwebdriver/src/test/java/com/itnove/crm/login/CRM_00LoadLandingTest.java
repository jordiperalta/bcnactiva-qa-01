package com.itnove.crm.login;

import com.itnove.crm.BaseTest;
import com.itnove.crm.pages.LoginPage;
import org.junit.Assert;
import org.junit.Test;

public class CRM_00LoadLandingTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        LoginPage login = new LoginPage(driver);

        // naveguem fins a la web CRM
        driver.navigate().to("http://crm.votarem.lu");

        // comprovem que la pagina de login es carrega
        Assert.assertTrue(login.isLoginPagePresent());
    }
}