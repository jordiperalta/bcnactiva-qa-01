package com.itnove.crm.login;

import com.itnove.crm.BaseTest;
import com.itnove.crm.pages.DashBoard;
import com.itnove.crm.pages.LoginPage;
import org.junit.Assert;
import org.junit.Test;

public class CRM_01LoginCorrectTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        LoginPage login = new LoginPage(driver);
        DashBoard dashboard = new DashBoard(driver);

        // naveguem fins a la web CRM
        driver.navigate().to("http://crm.votarem.lu");

        // comprovem que la pagina de login es carrega
        Assert.assertTrue(login.isLoginPagePresent());

        // introduim usuari i password correcte
        login.loginSubmit("user","bitnami");

        // comprovem que la pagina de dashboard es carrega
        Assert.assertTrue(dashboard.isDashboardPresent(driver, wait));
    }
}