package com.itnove.crm.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by guillem on 01/03/16.
 */
public class CreateAccount {

    private WebDriver driver;

    @FindBy(xpath = "//*[@id='pagecontent']/div[1]/h2")
    public WebElement moduleTitle;

    @FindBy(xpath = "//*[@id='ajaxHeader']/nav/div/div[5]/ul/li/a")
    public WebElement createDropdown;

    @FindBy(xpath = "//*[@id='ajaxHeader']/nav/div/div[5]/ul/li/ul/li[1]/a")
    public WebElement createAccountButton;

    @FindBy(id = "name")
    public WebElement inputNameField;

    @FindBy(id = "Accounts0emailAddress0")
    public WebElement inputEmailField;

    @FindBy(id = "SAVE")
    public WebElement saveButton;

    @FindBy(xpath = "//*[@id='pagecontent']/div/h2")
    public WebElement savedAccountLabel;

    @FindBy(xpath = "//*[@id='tab-actions']/a")
    public WebElement actionsDroplistTab;

    @FindBy(xpath = "//*[@id='tab-actions']/ul/li/input[@id='delete_button']")
    public WebElement deleteElementButton;

    public CreateAccount(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public boolean isPageRequestedPresent(String s) throws InterruptedException {

        System.out.println("CreateAccount.java:50 "+moduleTitle.getText());
        return (
            moduleTitle.isDisplayed() &&
            moduleTitle.getText().contains(s)
        );
    }

    public void fillRequiredFields(WebDriver driver, String s) throws InterruptedException {

        String fieldXPath = "//*[@class='required']/../../div/input";
        List<WebElement> fields = driver.findElements(By.xpath(fieldXPath));
        for (int i = 1; i < fields.size() + 1; i ++) {
            WebElement field = driver.findElement(By.xpath(fieldXPath+"["+i+"]"));
            field.sendKeys(s);
            Thread.sleep(1000);
            saveButton.click();
        }
    }

    public void deleteElement(WebDriver driver) throws InterruptedException {

        actionsDroplistTab.click();
        Thread.sleep(500);
        deleteElementButton.click();
        Thread.sleep(500);
        driver.switchTo().alert().accept();
    }

    public void setInputNameField(String data) {

        inputNameField.clear();
        inputNameField.sendKeys(data);
    }

    public void setInputEmailField(String data) {

        inputEmailField.clear();
        inputEmailField.sendKeys(data);
    }
}
