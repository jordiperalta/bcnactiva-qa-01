package com.itnove.crm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class LoginPage {

    private WebDriver driver;

    @FindBy(id = "user_name")
    public WebElement loginUserName;

    @FindBy(id = "user_password")
    public WebElement loginUserPass;

    @FindBy(id = "bigbutton")
    public WebElement loginButton;

    @FindBy(xpath = "//*[@id='form']/span[3]")
    public WebElement loginError;

    @FindBy(xpath = "//*[@class='desktop-bar']/ul/li[@id='globalLinks']/button[1]")
    public WebElement userMenuIcon;

    @FindBy(xpath = "//*[@class='desktop-bar']/ul/li[@id='globalLinks']/ul/li/a[@id='logout_link']")
    public WebElement logoutLink;


    public boolean isLoginPagePresent() {
        return (
            loginUserName.isDisplayed() &&
            loginUserPass.isDisplayed() &&
            loginButton.isDisplayed()
        );
    }

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void loginSubmit(String username, String password) {
        loginUserName.clear();
        loginUserPass.clear();
        loginUserName.sendKeys(username);
        loginUserPass.sendKeys(password);
        loginButton.click();
    }

    public boolean isErrorMessagePresent(WebDriver driver,WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(loginError));
        return ( loginError.isDisplayed() );
    }

    public String errorMessageDisplayed() {
        String errorMessage = "";
        if (loginError.isDisplayed()) {
            errorMessage = loginError.getText();
        }
        return errorMessage;
    }

    public void logoutClick(Actions action,WebDriverWait wait) {
        action.moveToElement(userMenuIcon).perform();
        wait.until(ExpectedConditions.visibilityOf(logoutLink));
        logoutLink.click();
    }

}
