package com.itnove.crm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by guillem on 01/03/16.
 */
public class DashBoard {

    private WebDriver driver;

    @FindBy(id = "tab0")
    public WebElement suiteCRMDashboard;

    @FindBy(xpath = "//*[@id='grouptab_0']")
    public WebElement salesDropdown;

    @FindBy(xpath = "//*[@id='grouptab_0']/../ul/li")
    public List<WebElement> salesMenuButtons;

    @FindBy(xpath = "//*[@id='grouptab_1']")
    public WebElement marketingDropdown;

    @FindBy(xpath = "//*[@id='grouptab_1']/../ul/li")
    public List<WebElement> marketingMenuButtons;

    @FindBy(xpath = "//*[@id='grouptab_4']")
    public WebElement collaborationDropdown;

    @FindBy(xpath = "//*[@id='grouptab_4']/../ul/li")
    public List<WebElement> collaborationMenuButtons;

    @FindBy(xpath = "//*[@id='toolbar'][@class='desktop-toolbar']")
    public WebElement desktopToolBar;

    @FindBy(xpath = "//*[@class='desktop-bar']/ul[@id='toolbar']")
    public WebElement userToolBar;

    @FindBy(xpath = "//*[@id='sidebar_container']/div[@class='sidebar']")
    public WebElement sideBar;

    @FindBy(xpath = "//*[@id='pagecontent']/div[@class='dashboard']")
    public WebElement divDashboard;

    public DashBoard(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public boolean isDashboardPresent(WebDriver driver,WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(suiteCRMDashboard));
        return ( suiteCRMDashboard.isDisplayed() );
    }
}
