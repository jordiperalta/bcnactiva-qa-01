package com.itnove.crm.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by guillem on 01/03/16.
 */
public class HoverLinks {

    public int iterations = 0;
    public int elements = 0;

    @FindBy(xpath = "//*[@id='toolbar'][@class='desktop-toolbar']/ul/li")
    public List<WebElement> desktopToolbarElements;

    @FindBy(xpath = "//*[@class='desktop-bar']/ul/li[@id='quickcreatetop']/a")
    public WebElement createDropdown;

    public HoverLinks(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void hoverDesktopToolbarDropLinks(WebDriver driver, Actions hover) throws InterruptedException {

        for (int i = 1; i < desktopToolbarElements.size() + 1; i ++) {
            WebElement desktopToolbarElement = driver.findElement(By.xpath("//*[@id='toolbar'][@class='desktop-toolbar']/ul/li["+i+"]"));
            hover.moveToElement(desktopToolbarElement).build().perform();
            Thread.sleep(500);

            List<WebElement> desktopToolbarDropLinks = driver.findElements(By.xpath("//*[@id='toolbar'][@class='desktop-toolbar']/ul/li["+i+"]/span/ul/li"));
            elements += desktopToolbarDropLinks.size();
            for (int j = 1; j < desktopToolbarDropLinks.size() + 1; j ++) {
                WebElement desktopToolbarDropLink = driver.findElement(By.xpath("//*[@id='toolbar'][@class='desktop-toolbar']/ul/li["+i+"]/span/ul/li["+j+"]"));
                hover.moveToElement(desktopToolbarDropLink).build().perform();
                Thread.sleep(500);
                iterations ++;
            }
        }
    }

    public int getIterations() {
        return iterations;
    }

    public int getElements() {
        return elements;
    }

    public void hoverCreateClickAccountLink (WebDriver driver, Actions hover, int index) throws InterruptedException {

        hover.moveToElement(createDropdown).build().perform();
        Thread.sleep(1000);
        WebElement createElementLink = driver.findElement(By.xpath("//*[@class='desktop-bar']/ul/li[@id='quickcreatetop']/ul/li["+index+"]/a"));
        createElementLink.click();
    }

}
