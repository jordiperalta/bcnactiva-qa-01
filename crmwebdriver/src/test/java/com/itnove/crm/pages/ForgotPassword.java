package com.itnove.crm.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by guillem on 01/03/16.
 */
public class ForgotPassword {

    private WebDriver driver;

    @FindBy(id = "fp_user_name")
    public WebElement forgotPwdUserName;

    @FindBy(id = "fp_user_mail")
    public WebElement forgotPwdUserEmail;

    @FindBy(id = "forgotpasslink")
    public WebElement forgotPassLink;

    @FindBy(id = "generate_pwd_button")
    public WebElement generatePwdButton;

    @FindBy(id = "generate_success")
    public WebElement generateSuccess;

    public ForgotPassword(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public boolean isForgotPwdFormPresent() {
        return (
            forgotPwdUserName.isDisplayed() &&
            forgotPwdUserEmail.isDisplayed() &&
            generatePwdButton.isDisplayed()
        );
    }

    public void forgotPasswordSubmit(String username, String email) {
        forgotPwdUserName.clear();
        forgotPwdUserEmail.clear();
        forgotPwdUserName.sendKeys(username);
        forgotPwdUserEmail.sendKeys(email);
        generatePwdButton.click();
    }

}
