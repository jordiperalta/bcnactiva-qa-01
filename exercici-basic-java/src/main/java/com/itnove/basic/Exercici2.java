package com.itnove.basic;

import java.util.Scanner;

public class Exercici2 {
    public static void main(String[] args) {

        System.out.println("******************* taula de multiplicar *******************");
        System.out.println("");

        Scanner s=new Scanner(System.in);
        int numero;
        System.out.println("Introdueix un numero enter");
        numero = s.nextInt();
        taulaMultiplicar(numero);
    }

    public static int taulaMultiplicar (int valor) {
        for (int i=0; i<11; i++){
            System.out.println(i+"x"+valor+" = "+i*valor);
        }
        return 0;
    }
}
