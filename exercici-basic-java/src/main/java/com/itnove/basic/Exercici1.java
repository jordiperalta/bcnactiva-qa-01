package com.itnove.basic;

import java.util.Scanner;

public class Exercici1 {
    public static void main(String[] args) {

        System.out.println("******************* calcul de graus Kelvin *******************");
        System.out.println("");

        Scanner s=new Scanner(System.in);
        String resposta="";
        double kelvin;
        double celsius;

        while (!resposta.equals("N")){
            System.out.println("Introdueix una temperatura en ºC");
            celsius = s.nextInt();
            kelvin = convertToKelvin(celsius);
            System.out.println("La temperatura es de "+kelvin+"K");

            System.out.println("Vols introduir un altre valor S/N?");
            resposta=String.valueOf(s.next().charAt(0)).toUpperCase();
        }
    }

    public static double convertToKelvin (double entrada){
        double sortida = entrada + 273.15;
        return sortida;
    }
}
