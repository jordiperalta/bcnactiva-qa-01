package com.itnove.basic;

import java.util.Arrays;
import java.util.Scanner;

public class CaesarCypher {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Introduzca una frase: ");
        String frase = sc.nextLine();

        System.out.print("Introduce un número para hacer el shift: ");
        int numShift = sc.nextInt();

        frase = normalizeText(frase); // guardamos frase normalizada
        System.out.println("\n\rFrase normalizada = " + frase);

        String shiftedAlphabet = shiftAlphabet(numShift); // guardamos alfabeto shifteado
        System.out.println("Alfabeto shifted = " + shiftedAlphabet);

        int [] arrayFrase = stringToArray(frase); // convierto frase normalizada a array de numeros (no se shiftea) ABCZ => [1, 2, 3, 26]
        String caesarified = arrayToCaesar(arrayFrase, shiftedAlphabet); // se usa alfabeto shifteado para asignar una letra a cada valor del array
        System.out.println("\n\rLa frase codificada es: " + caesarified);
    }

    public static String normalizeText(String frase) {
        
        frase = frase.trim();
        frase = frase.toUpperCase();
        frase = frase.replaceAll("[ .,;'\"`¡-]", "");
        return frase;

    }

    public static int [] stringToArray (String shifted) {

        int[] arrayOfShifted = new int[shifted.length()]; // inicialitzem un array amb la llargada del string introduït

        for (int i = 0; i < shifted.length(); i++){
            arrayOfShifted[i] = (int) shifted.charAt(i) - 65; // emmagatzemem el valor númeric de cada lletra i restem 65 ja que (int)'A' = 65
            // System.out.print(arrayShifted[i]+", "); // Podem imprimir el valor de cada caracter
        }
        // System.out.print("");
        return arrayOfShifted;
    }

    public static String arrayToCaesar (int [] arrayOfShifted, String shiftedAlphabet) {

        String stringifiedArray = "";

        for (int i = 0; i < arrayOfShifted.length; i++){
            stringifiedArray += shiftedAlphabet.charAt(arrayOfShifted[i]);
        }

        return stringifiedArray;
    }

    public static String shiftAlphabet(int shift){

        int start;
        if (shift < 0) {
            start = (int) 'Z' + shift + 1;
        } else {
            start = 'A' + shift;
        }
        String result = "";
        char currChar = (char) start;
        for(; currChar <= 'Z'; ++currChar) {
            result = result + currChar;
        }
        if(result.length() < 26) {
            for(currChar = 'A'; result.length() < 26; ++currChar) {
                result = result + currChar;
            }
        }
        return result;

    }

}
