package com.itnove.basic;

import java.util.Scanner;

public class BattleShip {

    private static int [][] board = new int[5][5];
    private static int [][] ships = new int[3][2];
    private static int [] shoot = new int[2];

    public static void main(String[] args) {
        System.out.println("************ Battleship java game ************");
        System.out.println("");
        int shotHit = 0;
        int attempts = 0;
        int shotInput;
        Scanner sc = new Scanner(System.in);
        String readShot;
        initBoard();
        showBoard();
        initShips();
        while (shotHit != 3){
            for (int i=0; i<2; i++){
                shotInput = 0;
                readShot = i == 0 ? "fila" : "columna";
                System.out.print("Introdueix un número de "+readShot+": ");
                while (shotInput == 0){
                    readShot = sc.next();
                    if (!readShot.matches("[1-5]")){
                        System.out.println("El valor ha d'estar entre 1 i 5.");
                    } else {
                        shoot[i] = Integer.parseInt(readShot) - 1;
                        shotInput ++;
                    }
                }
            }
            if (shotHit(shoot)){
                System.out.println("Has encertat!!!");
                System.out.println("");
                shotHit ++;
            } else {
                hint(shoot);
            }
            changeBoard(shoot);
            showBoard();
            attempts ++;
        }
        System.out.println("Battleship java game finished! Has enfonsat 3 vaixells en "+ attempts +" intents.");
    }

    private static String cellSymbols(int cell) {
        switch (cell){
            case 1:
                return "x";
            case 0:
                return "*";
            default:
                return "~";
        }
    }

    private static void initBoard(){
        for (int i=0; i<5; i++){
            for (int j=0; j<5; j++){
                board[i][j]=-1;
            }
        }
    }

    private static void initShips(){
        for (int i=0; i<3; i++ ){
            for (int j=0; j<2; j++ ){
                ships[i][j] = (int)(Math.random() * 4);
            }
            for (int k=0; k<i; k++){
                if (ships[k][0] == ships[i][0] && ships[k][1] == ships[i][1]){
                    k = i;
                    i --;
                }
            }
        }
    }

    private static void showBoard() {
        System.out.println("    1   2   3   4   5");
        for (int i=0; i<5; i++){
            System.out.print((i+1)+"   ");
            for (int j=0; j<5; j++){
                System.out.print((cellSymbols(board[i][j]))+"   ");
            }
            System.out.println("");
        }
        System.out.println("");
    }

    private static boolean shotHit(int [] shoot) {
        for (int i=0; i<3; i++){
            if (ships[i][0] == shoot[0] && ships[i][1] == shoot[1])
                return true;
        }
        return false;
    }

    private static void changeBoard(int [] shoot) {
        if (shotHit(shoot)){
            board[shoot[0]][shoot[1]] = 1;
        } else {
            board[shoot[0]][shoot[1]] = 0;
        }
    }

    private static void hint(int [] shoot) {
        int coincidenceRow = 0, coincidenceColumn = 0;
        for (int i=0; i<3; i++){
            if (ships[i][0] == shoot[0])
                coincidenceRow ++;
            if (ships[i][1] == shoot[1])
                coincidenceColumn ++;
        }
        System.out.println("Fila " + (shoot[0]+1) + ": " + coincidenceRow + " vaixell/s");
        System.out.println("Columna " + (shoot[1]+1) + ": " + coincidenceColumn + " vaixell/s");
    }
}
