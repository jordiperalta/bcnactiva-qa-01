package com.itnove.basic;

import java.util.Scanner;

public class Exercici3 {
    public static void main(String[] args) {
        int suma = 0;
        int num;

        Scanner s=new Scanner(System.in);
        System.out.println("Introdueix un numero enter");
        num = s.nextInt();

        for (int i=2; i<=num; i++){
            if (num%i == 0) {
                suma += (num/i);
            }
        }

        if (num == suma) {
            System.out.println("numero molon");
        } else {
            System.out.println("numero no tan molon");

        }
    }
}
