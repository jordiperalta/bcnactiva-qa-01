package com.itnove.basic;

import java.util.Scanner;

public class Exercici4 {
    public static void main(String[] args) {
        int n=1;
        for (int i=1; i<100; n++){
            if (esNumeroPrimo(n)){
                System.out.print(n+" - ");
                i++;
                if (i%20==0) System.out.println("");
            }
        }
    }

    public static boolean esNumeroPrimo (int num){
        int suma = 0;

        for (int i=2; i<num; i++){
            if (num%i == 0) {
                suma++;
            }
        }
        return suma==0;
    }
}

