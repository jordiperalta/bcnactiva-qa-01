package com.itnove.basic;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BaseTest {
    private RomanNumeral romanTest = new RomanNumeral();

    @Test
    public void testIsValidXII() throws Exception {
        assertTrue(romanTest.isValid("XII"));
    }

    @Test
    public void testIsNotValidIIII() throws Exception {
        assertFalse(romanTest.isValid("IIII"));
    }

    @Test
    public void testIsNotValidVV() throws Exception {
        assertFalse(romanTest.isValid("VV"));
    }

    @Test
    public void testXIVis14() throws Exception {
        assertEquals(14, romanTest.convert("XIV"));
    }

    @Test
    public void testCMis900() throws Exception {
        assertEquals(900, romanTest.convert("CM"));
    }
}
