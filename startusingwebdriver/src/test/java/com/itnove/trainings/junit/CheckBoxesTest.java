package com.itnove.trainings.junit;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CheckBoxesTest extends BaseTest {

    /*
     * Test 1:
     * Obre herokuapp chekboxes
     * Assert checkbox1 unchecked
     * Assert checkbox2 checked
     * Click checkbox1
     * Assert checkbox1 checked
     */
    @Test
    public void testCase0X_SearchFoundItemsList() throws InterruptedException {

        // 1 obrim el web i esperem que es carregui un element de la pagina
        driver.navigate().to("http://the-internet.herokuapp.com/checkboxes");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='page-footer']/div/div")));

        // 2 compropvem que checkbox2 esta seleccionat
        WebElement checkBox2 = driver.findElementByXPath(".//*[@id='checkboxes']/input[2]");
        Assert.assertTrue(checkBox2.getAttribute("checked").equals("true"));

        // 3 compropvem que checkbox1 esta no seleccionat
        WebElement checkBox1 = driver.findElementByXPath(".//*[@id='checkboxes']/input[1]");
        Assert.assertNull(checkBox1.getAttribute("checked"));

        // 4 Fem click a checkbox1
        checkBox1.click();

        // 5 compropvem que checkbox1 esta seleccionat
        Assert.assertTrue(checkBox1.getAttribute("checked").equals("true"));

    }

}
