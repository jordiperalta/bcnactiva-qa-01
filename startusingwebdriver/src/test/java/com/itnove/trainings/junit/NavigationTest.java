package com.itnove.trainings.junit;

import org.junit.Test;


/**
 * Unit test for simple App.
 */
public class NavigationTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        driver.navigate().to("http://www.sport.es");
        driver.navigate().refresh();
        driver.navigate().to("http://www.mundodeportivo.com/");
        driver.navigate().back();
        driver.navigate().forward();
        driver.navigate().refresh();
    }
}
