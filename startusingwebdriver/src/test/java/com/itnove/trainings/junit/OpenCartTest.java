package com.itnove.trainings.junit;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OpenCartTest extends BaseClassTest{

    /*
     * Test 1: Carregar web e-commerce 'Your Store'
     */
    @Test
    public void testCase01_LandingPageLoaded() throws InterruptedException {
        driver.navigate().to("http://opencart.votarem.lu/");
        Thread.sleep(1000);
        Assert.assertEquals("Your Store", driver.getTitle());
        System.out.println("TC1.- Pàgina 'Your Store' carregada");
    }

    /*
     * Test 2: Comprovar que genera una llista en fer cerca de producte existent
     */
    @Test
    public void testCase02_SearchFoundItemsList() throws InterruptedException {
        WebElement element = driver.findElementByXPath("//*[@id='search']/input");
        element.sendKeys("Samsung");
        Thread.sleep(1000);
        driver.findElementByXPath("//*[@id='search']/span/button").click();
        Thread.sleep(1000);
        Assert.assertTrue(driver.findElementByClassName("product-layout").isDisplayed());
        System.out.println("TC2.- Llista d'elements cercats existent");
    }

    /*
     * Test 3: Comprovar que no es genera una llista en fer cerca de producte no existent
     */
    @Test
    public void testCase03_SearchNotFoundList() throws InterruptedException {
        driver.navigate().to("http://opencart.votarem.lu/");
        Thread.sleep(1000);
        WebElement element = driver.findElementByXPath("//*[@id='search']/input");
        element.sendKeys("mobile");
        Thread.sleep(1000);
        driver.findElementByXPath("//*[@id='search']/span/button").click();
        Thread.sleep(1000);
        Assert.assertTrue(driver.findElementsByClassName("product-layout").size() == 0);
        System.out.println("TC3.- Element de cerca no existent");
    }

    /*
     * Test 4: Comprovar que es genera una llista en fer click a un menu
     */
    @Test
    public void testCase04_ExistingItemsList() throws InterruptedException {
        WebElement element = driver.findElementByXPath("//*[@id='menu']/div[2]/ul/li[3]/a");
        element.click();
        Thread.sleep(500);
        element = driver.findElementByXPath("//*[@id='menu']/div[2]/ul/li[3]/div/div/ul/li[2]/a");
        element.click();
        Thread.sleep(500);
        Assert.assertTrue(driver.findElementsByClassName("product-layout").size() > 0);
        Thread.sleep(1000);
        System.out.println("TC4.- Llista d'elements cercats existent");
    }

    /*
     * Test 5: Comprovar que no es genera una llista en fer click a un menu amb items = 0
     */
    @Test
    public void testCase05_NonExistingItemList() throws InterruptedException {
        WebElement element = driver.findElementByXPath("//*[@id='menu']/div[2]/ul/li[2]/a");
        element.click();
        Thread.sleep(500);
        element = driver.findElementByXPath("//*[@id='menu']/div[2]/ul/li[2]/div/div/ul/li[2]/a");
        element.click();
        Thread.sleep(1000);
        Assert.assertTrue(driver.findElementsByClassName("product-layout").size() == 0);
        System.out.println("TC5.- Llista d'elements cercats no existent");
    }

    /*
     * Test 6: Comprovar que es genera una llista en fer click a un menu
     */
    @Test
    public void testCase06_ItemAddedToCart() throws InterruptedException {
        WebElement element = driver.findElementByXPath("//*[@id='menu']/div[2]/ul/li[1]/a");
        element.click();
        Thread.sleep(500);
        element = driver.findElementByXPath("//*[@id='menu']/div[2]/ul/li[1]/div/div/ul/li[2]/a");
        element.click();
        Thread.sleep(1000);
        element = driver.findElementByXPath("//*[@id=\"content\"]/div[2]/div/div/div[1]/a");
        element.click();
        Thread.sleep(1000);
        element = driver.findElementByXPath("//*[@id='button-cart']");
        element.click();
        Thread.sleep(500);
        element = driver.findElementByXPath("//*[@id='cart']/button");
        element.click();
        Thread.sleep(1000);
        element = driver.findElementByXPath("//*[@id='cart']/ul/li[2]/div/table/tbody/tr[4]/td[2]");
        Assert.assertEquals("$122.00",element.getText());
        System.out.println("TC6.- Element iMac afegit al cistell amb preu $122.00");
    }

    /*
     * Test 7: Comprovar que es genera una llista en fer click a un menu
     */
    @Test
    public void testCase07_ItemAddedToCart() throws InterruptedException {
        // 1
        driver.navigate().to("http://opencart.votarem.lu/");
        // 2
        Actions hover = new Actions(driver);
        WebElement element = driver.findElementByXPath("//*[@id='top-links']/ul/li[2]/a");
        hover.moveToElement(element).build().perform();
        Thread.sleep(2500);
        // 3
        element.click();
        Thread.sleep(500);
        // 4
        WebElement register = driver.findElementByXPath("//*[@id='top-links']/ul/li[2]/ul/li[1]/a");
        hover.moveToElement(register).build().perform();
        Thread.sleep(2500);
        // 5
        register.click();
        Thread.sleep(3500);
        // 6
        WebElement registerAccount = driver.findElementById("account-register");
        Assert.assertTrue(registerAccount.isDisplayed());
        System.out.println("TC7.- Pagina de registre carregada");

    }

}

