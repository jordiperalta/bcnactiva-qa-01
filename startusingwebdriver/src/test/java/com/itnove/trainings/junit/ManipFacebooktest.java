package com.itnove.trainings.junit;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.assertTrue;

public class ManipFacebooktest extends BaseTest{

    @Test
    public void testApp() throws InterruptedException {
        // 1
        driver.navigate().to("https://www.facebook.com");
        assertTrue(driver.getCurrentUrl().contains("facebook"));
        Thread.sleep(3000);
        // 2
        WebElement textUser = driver.findElementById("email");
        textUser.click();
        Thread.sleep(1000);
        // 3
        textUser.sendKeys("bcn9jpf@yahoo.es");
        Thread.sleep(3000);
        // 4
        WebElement textPass = driver.findElementById("pass");
        textPass.click();
        Thread.sleep(1000);
        // 5
        byte[] encodedBytes = Base64.decodeBase64("cmVsNEthdzE=".getBytes());
        String password = new String(encodedBytes);
        textPass.sendKeys(password);
        Thread.sleep(1000);
        // 6
        WebElement botoLogin = driver.findElementById("loginbutton");
        botoLogin.click();
        Thread.sleep(1000);
        // 7
        WebElement mur = driver.findElementByXPath("//*[@id='contentArea']");
        assertTrue(mur.isDisplayed());
    }

}
