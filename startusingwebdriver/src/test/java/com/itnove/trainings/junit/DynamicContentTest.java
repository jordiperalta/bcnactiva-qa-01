package com.itnove.trainings.junit;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class DynamicContentTest extends BaseTest{

    @Test
    public void testApp() throws InterruptedException {
        String image1, image2, text1, text2;
        int cycles = 20;
        int imageChanges = 0;
        int textChanges = 0;

        driver.navigate().to("https://the-internet.herokuapp.com/dynamic_content");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='content']/div/h3")));

        image1 = driver.findElementByXPath(".//*[@id='content']/div[3]/div[1]/img").
                getAttribute("src");
        text1 = driver.findElementByXPath(".//*[@id='content']/div[3]/div[2]").
                getText();

        for (int i = 0; i < cycles; i ++) {
            Thread.sleep(500);

            driver.navigate().refresh();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='content']/div/h3")));

            image2 = driver.findElementByXPath(".//*[@id='content']/div[3]/div[1]/img").
                    getAttribute("src");
            text2 = driver.findElementByXPath(".//*[@id='content']/div[3]/div[2]").
                    getText();

            if (!image1.equals(image2))
                imageChanges ++;
            if (!text1.equals(text2))
                textChanges ++;

            text1 = text2;
            image1 = image2;
        }
        System.out.println("cycles: "+cycles+" - imageChanges: "+imageChanges+" - textChanges: "+textChanges);
        Assert.assertTrue(imageChanges == cycles);
        Assert.assertTrue(textChanges == cycles);
    }
}
