package com.itnove.trainings.junit;

import org.junit.Test;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class DisappearingElementsTest extends BaseTest{

    @Test
    public void testApp() throws InterruptedException {

        int timesClicked = 0;
        int iterations = 20;
        driver.navigate().to("https://the-internet.herokuapp.com/disappearing_elements");

        for (int i = 0; i < iterations; i++){
            List<WebElement> buttons = driver.findElementsByXPath("//*[@id='content']/div/ul/li");
            WebElement lastButton = driver.findElementByXPath("//*[@id='content']/div/ul/li["+buttons.size()+"]");
            lastButton.click();
            driver.navigate().back();
            driver.navigate().refresh();
            timesClicked++;
            driver.manage().deleteAllCookies();
        }
        assertTrue(timesClicked == iterations);
    }
}
