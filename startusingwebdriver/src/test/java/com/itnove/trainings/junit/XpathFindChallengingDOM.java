package com.itnove.trainings.junit;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class XpathFindChallengingDOM extends BaseTest{

    /*
     * Test: Clickar sobre els botons existents i comprovar que es carrega la pagina
     */
    @Test
    public void testCase0X_CheckMenuHoverAndClick() throws InterruptedException {

        // 1 obrim el web i esperem que es carregui un element de la pagina
        driver.navigate().to("https://the-internet.herokuapp.com/challenging_dom");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@class='example']")));

        List<WebElement> botons = driver.findElementsByXPath(".//*[@class='example']/div/div/div[1]/a");

        for (int i = 1; i < botons.size() + 1; i++){

            WebElement boto = driver.findElementByXPath(".//*[@class='example']/div/div/div[1]/a["+i+"]");
            hover.moveToElement(boto).build().perform();
            Thread.sleep(350);
            boto.click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@class='example']")));

            String title = driver.findElementByXPath("//*[@id='content']/div/h3").getText();
            Assert.assertTrue(title.contains("Challenging DOM"));

            System.out.println("Button "+i+" clicked!");
        }

    }

}
