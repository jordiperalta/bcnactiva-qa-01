package com.itnove.trainings.junit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.io.IOException;


/**
 * Created by jperalta on 13/11/17.
 */
public class BaseClassTest {
    public static RemoteWebDriver driver; // for Chrome & Firefox
//    public HtmlUnitDriver driver; // for HtmlUnit

    @BeforeClass
    public static void setUp() throws IOException {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        System.setProperty("webdriver.chrome.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator + "chromedriver-linux");
        driver = new ChromeDriver(capabilities);
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }

}