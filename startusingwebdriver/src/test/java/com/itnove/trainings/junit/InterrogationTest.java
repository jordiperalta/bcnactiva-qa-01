package com.itnove.trainings.junit;

import org.junit.Test;

import static org.junit.Assert.assertTrue;


/**
 * Unit test for simple App.
 */
public class InterrogationTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        driver.navigate().to("http://www.sport.es");
        assertTrue(driver.getTitle().contains("SPORT"));
        System.out.println(driver.getTitle());
        assertTrue(driver.getPageSource().contains("Valverde"));
        driver.navigate().to("http://elmundodeportivo.com");
        assertTrue(driver.getTitle().contains("Mundo Deportivo"));
        System.out.println(driver.getTitle());
        assertTrue(driver.getCurrentUrl().contains("www.mundodeportivo.com"));
        assertTrue(driver.getPageSource().contains("Neymar"));
    }
}
