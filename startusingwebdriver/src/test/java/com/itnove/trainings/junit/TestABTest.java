package com.itnove.trainings.junit;

import org.junit.Test;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.assertTrue;

public class TestABTest extends BaseTest{

    @Test
    public void testApp() throws InterruptedException {

        int counter1 = 0;
        int counter2 = 0;
        int total = 500;
        for (int i = 0; i < total; i++){
            driver.navigate().to("https://the-internet.herokuapp.com/abtest");
            WebElement title = driver.findElementByXPath(".//*[@id='content']/div/h3");
            if(title.getText().equals("A/B Test Variation 1")){
                counter1 ++;
            } else {
                counter2 ++;
            }
//            assertTrue(title.getText().equals("A/B"));
            driver.manage().deleteAllCookies();
        }
        assertTrue(counter1+counter2==total);
    }
}
