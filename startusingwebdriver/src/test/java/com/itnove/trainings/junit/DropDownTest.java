package com.itnove.trainings.junit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class DropDownTest extends BaseTest{

    @Test
    public void testApp() throws InterruptedException {

        driver.navigate().to("https://the-internet.herokuapp.com/dropdown");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='dropdown']")));

        WebElement select = driver.findElementByXPath(".//*[@id='dropdown']");

        List<WebElement> options = driver.findElementsByXPath(".//*[@id='dropdown']/option");
        for (int i = 2; i < options.size()+1; i++){
            select.click();
            Thread.sleep(1000);

            WebElement option = driver.findElementByXPath(".//*[@id='dropdown']/option["+i+"]");
            option.click();
            Thread.sleep(1000);
        }
    }
}
