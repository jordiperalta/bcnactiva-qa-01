package com.itnove.trainings.junit;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class SearchWaitTest extends BaseTest {

    /*
     * Test 1: Comprovar que genera una llista en fer cerca de producte existent
     */
    @Test
    public void testCase0X_SearchFoundItemsList() throws InterruptedException {
        String key = "Mac";

        // 1 obrim el web i esperem que es carregui un element de la pagina
        driver.navigate().to("http://opencart.votarem.lu/");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='search']/input")));

        // 2 escrivim "Mac" a searchInput
        WebElement searchInput = driver.findElementByXPath("//*[@id='search']/input");
        searchInput.sendKeys(key);

        // 3 fer hover sobre searchButton
        WebElement searchButton = driver.findElementByXPath("//*[@id='search']/span/button");
        hover.moveToElement(searchButton).build().perform();
        Thread.sleep(500);

        // 4 fer click sobre searchButton
        searchButton.click();

        // 5 comprovem que es carrega la pagina amb x elements mostrats
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='content']/div[3]/div")));
        WebElement searchCriteria = driver.findElementByXPath(".//*[@id='content']/h1");
        String keyword = searchCriteria.getText();
        Assert.assertTrue(keyword.contains(key));

        WebElement inputSearch = driver.findElementByXPath(".//*[@id='input-search']");
        Assert.assertTrue(keyword.contains(key));

        List<WebElement> resultats = driver.findElementsByXPath(".//*[@id='content']/div[3]/div");
        for (int i = 1; i < resultats.size() + 1; i ++){
            WebElement titol = driver.findElementByXPath(".//*[@id='content']/div[3]/div["+i+"]/div/div[2]/div[1]/h4/a");
            Assert.assertTrue(titol.getText().toLowerCase().contains(key.toLowerCase()));
        }
        int elements = driver.findElementsByXPath(".//*[@id='content']/div[3]/div").size();
        Assert.assertTrue(elements > 0);
        System.out.println("Llista d'elements cercats existent. Nombre d'elements "+elements);

    }

}
