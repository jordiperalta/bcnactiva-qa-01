package com.itnove.trainings.junit;

import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class DragAndDropTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {


        driver.navigate().to("https://the-internet.herokuapp.com/drag_and_drop"); // check example at eviltester
        WebElement columnA = driver.findElementById("column-a"); // id will be different
        WebElement columnB = driver.findElementById("column-b"); // id will be different

        Actions dragAndDrop = new Actions(driver);
        dragAndDrop.dragAndDrop(columnA,columnB).perform();

        Thread.sleep(2500);
    }

}
