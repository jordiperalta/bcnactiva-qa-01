package com.itnove.trainings.junit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static junit.framework.TestCase.assertTrue;


/**
 * Unit test for simple App.
 */
public class RegisterTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        //1
        driver.get("http://opencart.votarem.lu");
        //2
        WebElement ninot = driver.findElement(By.xpath(".//*[@id='top-links']/ul/li[2]/a"));
        hover.moveToElement(ninot).build().perform();
//        Thread.sleep(500);
        //3
        ninot.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='top-links']/ul/li[2]/ul/li[1]/a")));
        //4
        WebElement register = driver.findElement(By.xpath(".//*[@id='top-links']/ul/li[2]/ul/li[1]/a"));
        hover.moveToElement(register).build().perform();
//        Thread.sleep(500);
        //5
        register.click();
        //6
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("account-register")));
        WebElement registerAccount = driver.findElement(By.id("account-register"));
        assertTrue(registerAccount.isDisplayed());



    }
}