package com.itnove.trainings.junit;

import org.junit.Test;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.assertTrue;

public class ManipulationTest extends BaseTest {

    @Test
    public void testApp() throws InterruptedException {
        // 1
        driver.navigate().to("https://www.duckduckgo.com");
        assertTrue(driver.getCurrentUrl().contains("duckduckgo"));
        Thread.sleep(3000);
        // 2
        WebElement textbotCerca = driver.findElementById("search_form_input_homepage");
        textbotCerca.click();
        Thread.sleep(3000);
        // 3
        textbotCerca.sendKeys("ibiza");
        Thread.sleep(3000);
        // 4
        WebElement lupa = driver.findElementById("search_button_homepage");
        lupa.click();
        Thread.sleep(3000);
        // 5.1
        assertTrue(driver.getTitle().contains("ibiza"));
        // 5.2
        WebElement linksCerca = driver.findElementById("links");
        assertTrue(linksCerca.isDisplayed());
        // 5.3
        WebElement linksLlistaCerca = driver.findElementByXPath(".//*[@id='r1-2']");
        linksLlistaCerca.click();
        Thread.sleep(2000);
        // 6
        assertTrue(!driver.getCurrentUrl().contains("duckduckgo"));


    }

}
