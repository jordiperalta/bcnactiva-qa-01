package com.itnove.trainings.junit;

        import org.junit.Assert;
        import org.junit.FixMethodOrder;
        import org.junit.Test;
        import org.junit.runners.MethodSorters;
        import org.openqa.selenium.By;
        import org.openqa.selenium.WebElement;
        import org.openqa.selenium.support.ui.ExpectedConditions;
        import org.openqa.selenium.support.ui.WebDriverWait;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OpenCartHoverTest extends BaseTest{

    /*
     * Test: Comprovar que s'activa hover en passar per sobre del menu
     */
    @Test
    public void testCase0X_CheckMenuHoverAndClick() throws InterruptedException {
        // 1
        driver.navigate().to("http://opencart.votarem.lu/");
        int elementsMenu = driver.findElementsByXPath(".//*[@id='menu']/div[2]/ul/li").size();
        System.out.println(elementsMenu);

        for (int i = 1; i < elementsMenu + 1; i++){
            WebElement element = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li["+i+"]/a");
            hover.moveToElement(element).build().perform();
            System.out.print("Menu "+i+" hovered. ");
            Thread.sleep(500);
            int showAllElements = driver.findElementsByXPath(".//*[@id='menu']/div[2]/ul/li["+i+"]/div/a").size();
            int subMenuColumns = driver.findElementsByXPath("//*[@id='menu']/div[2]/ul/li["+i+"]/div/div/ul").size();

            for (int j = 1; j < subMenuColumns + 1; j++){
                int subElements = driver.findElementsByXPath(".//*[@id='menu']/div[2]/ul/li["+i+"]/div/div/ul["+j+"]/li").size();
                for (int k = 1; k < subElements + 1; k++){
                    WebElement subelement = driver.findElementByXPath(".//*[@id='menu']/div[2]/ul/li["+i+"]/div/div/ul["+j+"]/li["+k+"]");
                    hover.moveToElement(subelement).build().perform();
                    Thread.sleep(250);
                }
            }

            if (showAllElements > 0) {
                WebElement selectAll = driver.findElementByXPath("//*[@id='menu']/div[2]/ul/li["+i+"]/div/a");
                hover.moveToElement(selectAll).build().perform();
                Thread.sleep(500);
                Assert.assertTrue(selectAll.isDisplayed());
                System.out.print("Submenu "+i+" displayed. ");
                selectAll.click();
                WebElement breadcrumb = (new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='product-category']/ul")));
                Thread.sleep(500);
                Assert.assertTrue(driver.findElementByXPath("//*[@id='product-category']/ul").isDisplayed());
                System.out.print("Elements of menu "+i+" displayed in new page. ");
            }
            System.out.println("");

        }

    }

}
