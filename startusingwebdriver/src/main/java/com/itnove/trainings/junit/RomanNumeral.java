package com.itnove.trainings.junit;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class RomanNumeral {

    private static Map<String, Integer> table;
    private String number;

    public RomanNumeral() {
        table = new HashMap<String, Integer>();
        table.put("I", 1);
        table.put("V", 5);
        table.put("X", 10);
        table.put("L", 50);
        table.put("C", 100);
        table.put("D", 500);
        table.put("M", 1000);
    }

    public static int convert(String number) {
        int suma=0;
        int operador;
        for(int i = 0; i < number.length(); i++)
        {
            operador = nextRoman(number, i);
            suma += table.get(String.valueOf(number.charAt(i)))*operador;
        }
        return suma;
    }

    public static boolean isValid(String number){
        if (!number.matches("[CDILMVX]+")){
            return false;
        }
        int repeat=0;
        int temp=0;
        for(int i = 0; i < number.length(); i++)
        {
            if (temp == table.get(String.valueOf(number.charAt(i)))) {
                repeat ++;
            } else {
                repeat = 0;
            }
            temp = table.get(String.valueOf(number.charAt(i)));

            if (repeat > 2) {
                return false;
            }
            if (repeat > 0 && (number.charAt(i) == 'V' || number.charAt(i) == 'L' || number.charAt(i) == 'D' )) {
                return false;
            }

            if (i < number.length()-1){
                if (table.get(String.valueOf(number.charAt(i))) * 10 < table.get(String.valueOf(number.charAt(i+1)))){
                    return false;
                }
            }
        }
        return true;
    }

    public static int nextRoman(String number, int i){
        if (i+1 < number.length()){
            if (table.get(String.valueOf(number.charAt(i))) < table.get(String.valueOf(number.charAt(i+1)))){
                return -1;
            }
        }
        return 1;
    }

    public static void main(String[] params){
        System.out.print( "Introdueix un número romà: " );
        Scanner in = new Scanner(System.in);

        // Reads a single line from the console
        // and stores into name variable
        RomanNumeral roman = new RomanNumeral();
        roman.number = in.nextLine().toUpperCase();

        if (roman.isValid(roman.number)){
            System.out.println( "El número romà " + roman.number + " equival a: " + roman.convert(roman.number));
        } else {
            System.out.println( "El número " + roman.number + " no és un número romà vàlid." );
        }
    }

}


